module.exports = {
  moduleNameMapper: {
    "\\.(css)$": "identity-obj-proxy"
  },
  setupFilesAfterEnv: ["<rootDir>/test-setup.js"],
  snapshotSerializers: ["enzyme-to-json/serializer"]
};
