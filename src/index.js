import React from 'react'
import ReactDOM from 'react-dom'

import Greeting from './components/greeting'
import Counter from './components/counter'

const Index = () => {
  return (
    <div>
      <Greeting name="World" />
      <Counter />
    </div>
  )
}

ReactDOM.render(<Index />, document.getElementById('app'))
