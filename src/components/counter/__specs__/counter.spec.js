import React from 'react'

import Counter from '..'

describe('<Counter />', () => {
  it('should render correctly with no value passed', () => {
    const component = render(<Counter />)

    expect(component).toMatchSnapshot()
  })

  it('should render correctly with a value passed in', () => {
    const component = render(<Counter value={10} />)

    expect(component).toMatchSnapshot()
  })

  describe('when +1 is clicked', () => {
    let component, button, span

    beforeAll(() => {
      component = mount(<Counter value={20} />)

      button = component.find('button')

      span = component.find('span')
    })

    it('should add 1 to the value', () => {
      button.simulate('click')

      expect(span.text()).toEqual('21')

      // .toMatchSnapshot() will get you there, but you have to be careful about too many snapshots
      expect(component).toMatchSnapshot()
    })

    it('should add 1 to the value again', () => {
      button.simulate('click')

      expect(span.text()).toEqual('22')

      expect(component).toMatchSnapshot()
    })
  })
})
