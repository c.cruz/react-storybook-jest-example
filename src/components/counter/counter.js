import React, { useState } from 'react'

import css from './counter.css'

const Counter = ({ value }) => {
  const [count, setCount] = useState(value || 0)

  return (
    <div className={css.container}>
      <span>{count}</span>
      <button onClick={() => setCount(count + 1)}>+1</button>
    </div>
  )
}

export default Counter
