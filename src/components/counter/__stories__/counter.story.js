import React from 'react'
import { storiesOf } from '@storybook/react'

import Counter from '..'

storiesOf('Components/Counter', module)
  .addDecorator(story => (
    <div
      style={{
        margin: '100px auto',
        width: '350px',
        border: '1px solid #00000022'
      }}
    >
      {story()}
    </div>
  ))
  .add('No initial value', () => <Counter />)
  .add('Initial value of 10', () => <Counter value={10} />)
