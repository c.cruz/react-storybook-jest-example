import React from 'react'
import { storiesOf } from '@storybook/react'

import Greeting from '..'

storiesOf('Components/Greeting', module)
  .addDecorator(story => (
    <div
      style={{
        margin: '100px auto',
        width: '350px',
        border: '1px solid #00000022'
      }}
    >
      {story()}
    </div>
  ))
  .add('Clinton', () => <Greeting name="Clinton" />)
  .add('Carlos', () => <Greeting name="Carlos" />)
  .add('David', () => <Greeting name="David" />)
