import React from 'react'

import css from './greeting.css'

const Greeting = ({ name }) => (
  <div className={css.container}>{`Hello ${name}!`}</div>
)

export default Greeting
