import React from 'react'

import Greeting from '..'

describe('<Greeting />', () => {
  it('should render the name correctly', () => {
    const component = render(<Greeting name="test-name" />)

    expect(component).toMatchSnapshot()
  })
})
