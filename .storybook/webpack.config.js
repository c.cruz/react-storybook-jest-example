const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const path = require("path");

const cssHashAlgorithm = "contenthash:20";

module.exports = async ({ config, mode }) => {
  // Remove the existing CSS rule
  config.module.rules = config.module.rules.filter(
    f => f.test.toString() !== "/\\.css$/"
  );

  config.module.rules.push({
    test: /\.css$/,
    use: [
      MiniCssExtractPlugin.loader,
      {
        loader: "css-loader",
        options: {
          modules: {
            localIdentName: "[local]--[hash:base64:5]"
          },
          importLoaders: 1
        }
      },
      {
        loader: "postcss-loader",
        options: {
          sourceMap: true
        }
      }
    ]
  });

  config.plugins.push(
    new MiniCssExtractPlugin({
      filename: `[name].[${cssHashAlgorithm}].css`,
      chunkFilename: `[name]-[id].css`
    })
  );

  return config;
};
